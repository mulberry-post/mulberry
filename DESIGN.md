- "Channel" is treated also like "Author"
- The "ChannelID" can be either:
  - `id_` followed by the cyprotgraphic public key
  - `nm_` followeb by a name that we'll use to resolve to a cryptographic public key (Not in scope right now)
- These aren't foreign keys because a channel can hold references to content that the peer doesn't know about

```mermaid
erDiagram
  Peer }|--o{ Channel: has
  Channel {
      ChannelID id
  }
  Peer }|--o{ Content: has
  Content {
    ContentID id
    ChannelID authorId
    string body
  }
  Channel }o--o{ Content: lists
  Channel }o--o| Content: readme
```
